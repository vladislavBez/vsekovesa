<?php
/**
 * NoiseInsulation functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NoiseInsulation
 */

if (!function_exists('noiseinsulation_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function noiseinsulation_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on NoiseInsulation, use a find and replace
         * to change 'noiseinsulation' to the name of your theme in all the template files.
         */
        load_theme_textdomain('noiseinsulation', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'noiseinsulation'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('noiseinsulation_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'noiseinsulation_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function noiseinsulation_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('noiseinsulation_content_width', 640);
}

add_action('after_setup_theme', 'noiseinsulation_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function noiseinsulation_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'noiseinsulation'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'noiseinsulation'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'noiseinsulation_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function noiseinsulation_scripts()
{
    wp_enqueue_style('noiseinsulation-style', get_stylesheet_uri());

    wp_enqueue_script('noiseinsulation-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('noiseinsulation-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'noiseinsulation_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Enabling theme support woocommerce
 */
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'woocommerce_support');

/**
 * Connecting your own styles
 */

wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap/css/bootstrap.min.css', array(), '1.0', null);
wp_register_style('bootstrap-theme', get_template_directory_uri() . '/assets/css/bootstrap/css/bootstrap-theme.min.css', array(), '1.0', null);
wp_register_style('main', get_template_directory_uri() . '/assets/css/accord/main.css', array("woocommerce-general", "woocommerce-layout"), '1.0', null);
wp_register_style('slick', get_template_directory_uri() . '/assets/css/slick/slick.css', array("woocommerce-general", "woocommerce-layout"), '1.0', null);
wp_register_style('header', get_template_directory_uri() . '/assets/css/accord/header.css', array("woocommerce-general", "woocommerce-layout"), '1.0', null);
wp_register_style('sliders', get_template_directory_uri() . '/assets/css/accord/sliders.css', array("woocommerce-general", "woocommerce-layout", "bootstrap"), '1.0', null);
wp_register_style('content-blocks', get_template_directory_uri() . '/assets/css/accord/content-blocks.css', array("woocommerce-general", "woocommerce-layout", "bootstrap"), '1.0', null);
wp_register_style('custom-checkbox', get_template_directory_uri() . '/assets/css/accord/custom-checkbox.css', array("woocommerce-general", "woocommerce-layout"), '1.0', null);
wp_register_style('shop', get_template_directory_uri() . '/assets/css/accord/shop.css', array("woocommerce-general", "woocommerce-layout", "woocommerce-smallscreen"), '1.0', null);
wp_register_style('custom-contact-form-7', get_template_directory_uri() . '/assets/css/accord/custom-contact-form-7.css', array(), '1.0', null);
wp_register_style('reset-joomla-styles', get_template_directory_uri() . '/assets/css/reset-joomla-styles.css', array("woocommerce-general", "woocommerce-layout", "woocommerce-smallscreen"), '1.0', null);

if (!is_admin()) {
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('bootstrap-theme');
    wp_enqueue_style('slick');
    wp_enqueue_style('header');
    wp_enqueue_style('sliders');
    wp_enqueue_style('content-blocks');
    wp_enqueue_style('custom-checkbox');
    wp_enqueue_style('main');
    wp_enqueue_style('shop');
    wp_enqueue_style('custom-contact-form-7');
    wp_enqueue_style('reset-joomla-styles');
}

/**
 * From JOOMLA styles
 */
wp_register_style('joomla-cart', get_template_directory_uri() . '/assets/css/cart.css', array(), '1.0', null);
wp_register_style('joomla-catalog', get_template_directory_uri() . '/assets/css/catalog.css', array(), '1.0', null);
wp_register_style('joomla-checkout', get_template_directory_uri() . '/assets/css/checkout.css', array(), '1.0', null);
wp_register_style('joomla-flypage', get_template_directory_uri() . '/assets/css/flypage.css', array(), '1.0', null);
wp_register_style('joomla-footer', get_template_directory_uri() . '/assets/css/footer.css', array(), '1.0', null);
wp_register_style('joomla-header_and_nav', get_template_directory_uri() . '/assets/css/header_and_nav.css', array(), '1.0', null);
wp_register_style('joomla-modal', get_template_directory_uri() . '/assets/css/modal.css', array(), '1.0', null);
wp_register_style('joomla-reset_and_ff', get_template_directory_uri() . '/assets/css/reset_and_ff.css', array(), '1.0', null);
wp_register_style('joomla-results', get_template_directory_uri() . '/assets/css/results.css', array(), '1.0', null);
wp_register_style('joomla-sections', get_template_directory_uri() . '/assets/css/sections.css', array(), '1.0', null);

if (!is_admin()) {
    wp_enqueue_style('joomla-cart');
    wp_enqueue_style('joomla-catalog');
    wp_enqueue_style('joomla-checkout');
    wp_enqueue_style('joomla-flypage');
    wp_enqueue_style('joomla-footer');
    wp_enqueue_style('joomla-header_and_nav');
    wp_enqueue_style('joomla-modal');
    wp_enqueue_style('joomla-reset_and_ff');
    wp_enqueue_style('joomla-results');   
    wp_enqueue_style('joomla-sections'); 
}

/**
 * Connecting your own scripts
 */
wp_register_script('bootstrap-js', get_template_directory_uri() . '/assets/js/slick/slick.min.js', array('jquery'), '1.0', true);
wp_register_script('slick', get_template_directory_uri() . '/assets/js/slick/slick.min.js', array('jquery'), '1.0', true);
wp_register_script('sliders', get_template_directory_uri() . '/assets/js/sliders.js', array(
    'jquery',
    'slick'
), '1.0', true);
wp_register_script('widgets', get_template_directory_uri() . '/assets/js/widgets.js', array(
    'jquery',
    "bootstrap-js"
), '1.0', true);
wp_register_script('image-tiles', get_template_directory_uri() . '/assets/js/image-tiles.js', array('jquery'), '1.0', true);
wp_register_script('parallax', get_template_directory_uri() . '/assets/js/parallax.js', array('jquery'), '1.0', true);

wp_deregister_script('jquery');
wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1/jquery-3.3.1.min.js', false, false, true);
wp_enqueue_script('jquery');

if (!is_admin()) {
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('slick');
    wp_enqueue_script('sliders');
    wp_enqueue_script('widgets');
    wp_enqueue_script('image-tiles');
    wp_enqueue_script('parallax');
}

/**
 * Register a hook to hidden the breadcrumb
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/**
 * Register a hook to display the custom breadcrumb
 */
add_action('woocommerce_custom_breadcrumb', 'woocommerce_breadcrumb', 5);

/**
 * Register a hook to display the custom price title
 */
function woocommerce_custom_price_title()
{
    echo "<span class='price-title'>Цена:</span>";
}

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_custom_price_title', 9);

/**
 * Register a hook to display the product description in list
 */

add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_single_excerpt', 15);

/**
 * Hook's removal of the name and price and add to cart
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

/**
 * Hook's removal of add to cart
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

/**
 * Addition of huk of the name and price to custom Hook
 */

add_action('woocommerce_custom_single_product_name_and_price','woocommerce_template_single_title', 5);
add_action('woocommerce_custom_single_product_name_and_price','woocommerce_template_single_price', 10);

/**
 * To add Hook's "addition to cart" to custom Hook
 */

add_action('woocommerce_custom_single_product_add_to_cart','woocommerce_template_single_add_to_cart', 30);

/**
 * Hook's removal of metadata
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

/**
 * Addition to custom Hook conclusion of metadata
 */

add_action('woocommerce_custom_single_product_meta','woocommerce_template_single_meta', 40);

/**
 * Output of the word price in the product card
 */

function custom_single_product_before () {
    echo "<div class='custom-single-product-price-wrapper'>";
}

function custom_single_product_after () {
    echo "</div>";
}

function custom_single_product_price_label () {
    echo "<p class='custom-single-product-price-label'>Цена:</p>";
}
add_action('woocommerce_custom_single_product_name_and_price','custom_single_product_before', 6);
add_action('woocommerce_custom_single_product_name_and_price','custom_single_product_price_label', 7);
add_action('woocommerce_custom_single_product_name_and_price','custom_single_product_after', 15);

/**
 * Output of the custom sale flash
 */

add_filter('woocommerce_sale_flash', 'my_custom_sale_flash', 10, 3);
function my_custom_sale_flash($text, $post, $_product) {
    return '<span class="onsale">Скидка 5%</span>';
}

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

/**
 * Output of block of goods under the card of a single product
 */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('custom_single_output_related_products','woocommerce_output_related_products', 20);