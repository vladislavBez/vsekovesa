<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NoiseInsulation
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<?php wp_head(); ?>
</head>
<!-- Yandex.Metrika counter -->
<!-- /Yandex.Metrika counter -->
<body id="main-body" <?php body_class(); ?>>
<div id="page" class="site">        
        
    <header id="masthead" class="site-header">

        <div class="container-mobile-header hidden-md hidden-lg">
            <div id="mobile-icon-menu" class="header-icon header-mobile-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24" enable-background="new 0 0 24 24">
                    <g>
                        <path d="M24,3c0-0.6-0.4-1-1-1H1C0.4,2,0,2.4,0,3v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V3z"></path>
                        <path d="M24,11c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V11z"></path>
                        <path d="M24,19c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V19z"></path>
                    </g>
                </svg>
            </div>
            <div class="header-mobile-logo">
                <a href="<?php echo get_home_url(); ?>" class="header-logo-link">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.jpg'?>" alt="text">
                </a>
            </div>
            <div class="header-icon header-mobile-icon">
                <a href="tel:+79301324075" class="reset-color">
                    <span class="glyphicon glyphicon-earphone"></span>
                </a>
            </div>
            
            <!--header tablet menu-->
            <div id="header-menu-widget" class="header-menu-widget-container">
                <div class="header-menu-widget">
                    <div class="header-tablet-menu-cart">
                        <div class="tablet-menu-cart">
                            <div class="tablet-menu-cart-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/shopping_cart.png'?>" alt="text">
                            </div>
                            <div class="tablet-menu-cart-info">
                                <div class="tablet-menu-cart-link">
                                    <a href="<?php echo get_page_link(7); ?>">Посмотреть корзину</a>
                                </div>
                                <div class="tablet-menu-cart-text">
                                    <div class="">
                                        <span id="tablet-cart-count">
                                        <?php
                                            echo WC()->cart->get_cart_contents_count();
                                        ?>
                                        </span>
                                        <span> - </span>
                                        <span id="tablet-cart-price">
                                        <?php
                                            echo WC()->cart->get_cart_subtotal();
                                        ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-menu-cart-button">
                            <a class="tablet-menu-cart-checkout" href="<?php echo get_page_link(8); ?>">Оформить заказ</a>
                        </div>
                    </div>
                    <div class="header-tablet-menu-list">
                        <?php wp_nav_menu('menu=MobileMenuFirst'); ?>
                    </div>
                    <div class="header-tablet-menu-categories">
                        <?php echo do_shortcode('[product_categories columns="1" orderby="term_group" parent="0"]'); ?>
                    </div>
                    <div class="header-tablet-menu-categories tablet-children-categories">
                        <?php echo do_shortcode('[product_categories columns="1" orderby="term_group" parent="21"]'); ?>
                    </div>
                    <div class="header-tablet-menu-list">
                        <?php wp_nav_menu('menu=MobileMenuSecond'); ?>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="hidden-xs hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-blocks">
                            <a href="/"><div id="logo"><div style="position:absolute; top:90px;color: #000;text-align:center;"> Интернет магазин шин и дисков в г. Ярославле</div></div></a>
                            <div id="auth">
                                <div id="auth_skype"><img src="<?php echo get_template_directory_uri() . '/assets/images/skype-color.png'?>" class="auth-header-icon-skype"/><a href="skype:vsekolesa?call"> Vsekolesa</a></div>
                                <div id="auth_icq"><img src="http://wwp.icq.com/scripts/online.dll?icq=637205774&img=27" alt="icq" /> 637 205 774</div>
                                <div id="auth_mail"><img src="<?php echo get_template_directory_uri() . '/assets/images/mail.png'?>" class="auth-header-icon-mail" alt="mail" /> 285-285@bk.ru</div>
                            </div>
                            <div id="cart">
                                <div id="cart_top" onclick="window.location = '/index.php?option=com_virtuemart&page=shop.cart'">КОРЗИНА</div>
                                <div id="cart_body"></div>
                            </div>
                            <div id="cont" style="margin: 10px 0 0 20px;">
                                    <p style="margin:0;">Пн-Пт 9.00-17.00, Сб., Вс. - Выходной</p>
                                    <p id="phone">
                                        <img src="<?php echo get_template_directory_uri() . '/assets/images/tel.png'?>" alt="телефон"/>
                                        <a href="tel:+74852285285">+7 (4852) 285-285</a>
                                    </p>
                                    <p style="color: red;font-weight: bold;">
                                        <span style="color: green;">Внимание: заказы принимаются только через корзину!</span><!--<strong>Интернет-магазин:</strong> ШИНЫ и ДИСКИ-->
                                    </p>
                                    <div id="address">
                                        <!--<a href="/kontakty"><strong>Самовывоз заказов,<br/>бесплатная доставка<br/> по Ярославлю от 4 шт.</strong></a>-->
                    <!--<a href="https://yandex.ru/maps/16/yaroslavl/?clid=2309398&ll=39.817944%2C57.685272&z=19&mode=poi&poi%5Buri%5D=ymapsbm1%3A%2F%2Forg%3Foid%3D186938061046&poi%5Bpoint%5D=39.817248%2C57.685305"  target="_blank"><strong>Самовывоз,<br/>бесплатная доставка<br/> по Ярославлю от 4 шт.</strong></a>-->
                    <a href="https://yandex.ru/maps/?um=constructor%3A643e6828858dc411bef289d3065a8e5eb1d8104205f8110c553b0a4b3e13183d&source=constructorLink" target="_blank">Самовывоз заказов<br>Ярославль, Автозаводская ул., 98В</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="konkurs-block">
                            <div id="konkurs" style="text-align: center;padding: 5px;float: left;margin-bottom: 5px;margin-top: 5px;margin-left: auto;margin-right: auto;background-color: rgba(243, 49, 49, 0.68);border: 3px solid #FF5959;border-style: outset;box-shadow: 4px 4px 2px rgba(214, 12, 12, 0.27);">
                              <a href="/akcii" target="blank" style="
                                /*margin-bottom: 5px;*/
                                font-size: 19px;
                                font-weight: bold;
                                text-align: center;
                                width: 100%;
                                color: #FFFFFF;
                                font-style: italic;
                                /* font-family: georgia; */
                                text-shadow: 2px 2px 3px rgb(142, 24, 24);
                                /* text-transform: uppercase; */
                                text-decoration: none;" id="blink6">Наши акции и скидки!</a>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-menu-block">
                            <div class="footer_nav">
                                <a href="<?php echo get_page_link(7); ?>">АКЦИИ</a>
                                <div class="devider">|</div>
                                <a href="<?php echo get_page_link(7); ?>">РАСПРОДАЖА</a>
                                <div class="devider">|</div>
                                <a href="<?php echo get_page_link(7); ?>">КРЕДИТ</a>
                                <div class="devider">|</div>
                                <a style="color: #ff9000 !important;    text-decoration: underline;" href="http://zapchasti76.ru/">АВТОЗАПЧАСТИ</a>                                
                                <div class="devider">|</div>
                                <a href="<?php echo get_page_link(7); ?>" style="/*text-decoration: underline;color: #fe8f01;*/"><span  style="color: #fe8f01;">&gt;&gt;</span> ПРАВИЛА ПОКУПКИ<span  style="color: #fe8f01;">&lt;&lt;</span></a>
                                <div class="devider">|</div>
                                <a href="http://gruzovye76.ru/" target="blank">ГРУЗОВЫЕ ШИНЫ</a>
                                <div class="devider">|</div>
                                <a href="<?php echo get_page_link(7); ?>">ГАРАНТИЯ</a>
                                <div class="devider">|</div>
                                <a href="<?php echo get_page_link(7); ?>">ДОСТАВКА</a>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </header><!-- #masthead -->
    
    <!--mobile fixed background-->
    <div id="mobile-fixed-background" class="hide"></div>

    <div class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="search">
                        <?php
                            if (is_front_page()):
                        ?>
                            <p class="search_main">ЛУЧШИЙ СПОСОБ КУПИТЬ КОЛЕСА, <span>ДЕЙСТВИТЕЛЬНО!</span></p>
                            <p class="search_main_2">Наш интернет-магазин предлагает большой выбор <strong>зимних</strong> и <strong>летних шин</strong> в Ярославле,<br />а также <strong>штампованных</strong> и <strong>литых дисков</strong> от лучших производителей мира.<br />Мы гарантируем, что наши <strong>шины и диски</strong> идеально подойдут для вашего автомобиля.</p>
                        <?php
                            else : 
                            echo '<div style="width: 100%; height: 10px;"></div>';
                            endif;
                        ?>
                            <div id="search_body">
                                <form id="search_tires" action="/index.php">
                                    <p>ПОДОБРАТЬ ШИНЫ</p>
                                    <ul>
                                    </ul> 
                                    <!--<div class="search_button">Найти</div>-->
                                    <input type="button" class="search_button" value="ПОИСК"/>
                                </form> 
                                <div class="search_devider"></div>
                                <form id="search_rims">
                                <p>ПОДОБРАТЬ ДИСКИ</p>
                                    <ul>
                                    <jdoc:include type="modules" name="top1" />
                                    </ul>
                                    <input type="button" class="search_button" value="ПОИСК"/>
                                </form>
                                <div class="search_devider"></div>
                                <form id="search_auto">
                                    <p>ПОДОБРАТЬ ПО АВТО</p>
                                    <div style="float: left; width: 100%;"><jdoc:include type="modules" name="top2" /></div>
                                </form>
                            </div>
                            <div style="opacity: 0">
                                <span>Д</span>
                            </div>
                        </div>
                        <div id="spec">
                            <div id="befrends" style="text-align: center; padding: 0px;">
                                <div>
                                    <span style="color: grey; margin-bottom: 0px; font-size: 12px !important; font-weight: bold;">Добавляйтесь к нам в соцсетях (у нас акции, новости, призы)</span>
                                </div>
                            <script type="text/javascript">(function(w, doc) {
                                            if (!w.__utlWdgt) {
                                                w.__utlWdgt = true;
                                                var d = doc,
                                                    s = d.createElement('script'),
                                                    g = 'getElementsByTagName';
                                                s.type = 'text/javascript';
                                                s.charset = 'UTF-8';
                                                s.async = true;
                                                s.src = ('https:' == w.location.protocol ? 'https' : 'http') + '://w.uptolike.com/widgets/v1/uptolike.js';
                                                var h = d[g]('body')[0];
                                                h.appendChild(s);
                                            }
                                        })(window, document);</script>
                            <div style="margin-top: -16px;" data-background-alpha="0.0" data-buttons-color="#FFFFFF" data-counter-background-color="#ffffff" data-share-counter-size="12" data-top-button="false" data-share-counter-type="disable" data-follow-ok="group/54985042886685" data-share-style="1" data-mode="follow" data-follow-vk="vsekolesa76" data-like-text-enable="false" data-follow-tw="vsekolesa76" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.vk.tw.ok.gp.mr.in.yt." data-share-size="30" data-background-color="#ffffff" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="1446497" data-counter-background-alpha="1.0" data-follow-gp="+vsekolesa76ru" data-follow-in="vsekolesa76" data-following-enable="false" data-exclude-show-more="false" data-follow-mr="community/vsekolesa76/" data-follow-yt="channel/UCAmZIBUqmv4dk6DGVC2bf3Q" data-selection-enable="false" data-follow-fb="vsekolesa76" class="uptolike-buttons">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    
    <div id="content" class="site-content">