<?php
/**
 * Template Name: Blog
 *
 * @package NoiseInsulation
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            
            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        
                        <?php
                            //Do not show the sidebar on the shopping cart page and place your order
                            $postid = get_the_ID();
                            if (($postid == 7) || ($postid == 8) || ($postid == 9) || wp_is_mobile()) {
                        ?>
                        <div class="col-sm-12 col-md-12">
                        <?php
                            } else {
                        ?>
                            <div class="col-sm-3 col-md-3">
                            <?php 
                            get_sidebar(); 
                            ?>
                        </div>
                        <div class="col-sm-9 col-md-9">
                        <?php 
                            }
                        ?>
                            <header class="entry-header">
                                <h1 class="entry-title">Наш блог</h1>  
                            </header>

                            <div class="entry-content">
                                <article>     
                                    <?php // Display blog posts on any page @ http://m0n.co/l
                                    $temp = $wp_query; $wp_query= null;
                                    $wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
                                    while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                             
                                    <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                                    <?php the_excerpt(); ?>
                             
                                    <?php endwhile; ?>
                             
                                    <?php if ($paged > 1) { ?>
                             
                                    <nav id="nav-posts">
                                        <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
                                        <div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
                                    </nav>
                             
                                    <?php } else { ?>
                             
                                    <nav id="nav-posts">
                                        <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
                                    </nav>
                             
                                    <?php } ?>
                             
                                    <?php wp_reset_postdata(); ?>
                                </article>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
