<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.

	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <div class="container">
		<?php
		do_action( 'woocommerce_custom_breadcrumb' );

		?>
        <div class="row">
            <div class="hidden-sm hidden-xs col-md-3">
				<div class="content-sidebar-fixed"> 
                    <?php get_sidebar("shop-sidebar"); ?>
               </div>
            </div>
            <div class="col-sm-12 col-md-9">
                <div class="container-single-product">
                    <?php
                    /**
                     * Hook: woocommerce_before_single_product_summary.
                     *
                     * @hooked woocommerce_show_product_sale_flash - 10
                     * @hooked woocommerce_show_product_images - 20
                     */
                    do_action( 'woocommerce_before_single_product_summary' );

                    ?>

                    <div class="summary entry-summary">
                        <div class="cart-prod-title-price-block">
                            <?php
                            /**
                             * Hook: woocommerce_custom_single_product_name_and_price.
                             * @hooked woocommerce_template_single_title - 5
                             * @hooked custom_single_product_before - 6
                             * @hooked custom_single_product_price_label - 7
                             * @hooked woocommerce_template_single_price - 10
                             * @hooked custom_single_product_after - 15
                             * @See function.php
                             */
                            do_action( 'woocommerce_custom_single_product_name_and_price' );
                            ?>
                        </div>

                        <?php
                        /**
                         * Hook: Woocommerce_single_product_summary.
                         * @hooked woocommerce_template_single_rating - 10
                         * @hooked woocommerce_template_single_excerpt - 20
                         * @hooked woocommerce_template_single_sharing - 50
                         * @hooked WC_Structured_Data::generate_product_data() - 60
                         */
                        do_action( 'woocommerce_single_product_summary' );
                        ?>
                        <div>
                        <?php
                        /**
                         * Hook: @hooked woocommerce_custom_single_product_meta.
                         * @hooked woocommerce_template_single_meta - 40
                         */
                        do_action( 'woocommerce_custom_single_product_meta' );
                        ?>
                        </div>
                        <div class="custom-single-product-add-cart-wrapper">
                            <?php
                            /**
                             * Hook: @hooked woocommerce_template_single_add_to_cart.
                             * @hooked woocommerce_template_single_add_to_cart - 30
                             */
                            do_action( 'woocommerce_custom_single_product_add_to_cart' );
                            ?>
                        </div>
                    </div>

                    <?php
                    /**
                     * Hook: woocommerce_after_single_product_summary.
                     *
                     * @hooked woocommerce_output_product_data_tabs - 10
                     * @hooked woocommerce_upsell_display - 15
                     * @hooked woocommerce_output_related_products - 20 -remove
                     * @See function.php
                     */
                    do_action( 'woocommerce_after_single_product_summary' );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="custom-single-related-products">
                    <?php
                    /**
                     * Hook: custom_single_output_related_products.
                     *
                     * @hooked woocommerce_output_related_products - 20
                     * @See function.php
                     */
                    do_action( 'custom_single_output_related_products' );
                    ?>
                </div>
            </div>
        </div>
    </div>
	<?php do_action( 'woocommerce_after_single_product' ); ?>
</div>
