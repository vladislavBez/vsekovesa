<?php
/**
 * Template Name: PagePaper
 *
 * @package NoiseInsulation
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            
            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        
                        <?php
                            //Do not show the sidebar on the shopping cart page and place your order
                            $postid = get_the_ID();
                            if (($postid == 7) || ($postid == 8) || ($postid == 9) || wp_is_mobile()) {
                        ?>
                        <div class="col-sm-12 col-md-12">
                        <?php
                            } else {
                        ?>
                            <div class="col-sm-3 col-md-3">
                            <?php 
                            get_sidebar(); 
                            ?>
                        </div>
                        <div class="col-sm-9 col-md-9">
                        <?php 
                            }
                        ?>
                        
                            <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                            ?>
                        </div>
                    </div>
                </div>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
